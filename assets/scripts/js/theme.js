// Load all the Custom Fonts

WebFont.load({
    google: {
      families: ['Raleway', 'Roboto Slab']
    }
  });

  (function(){
      // Vertical Timeline - by CodyHouse.co
  	function VerticalTimeline( element ) {
  		this.element = element;
  		this.blocks = this.element.getElementsByClassName("js-cd-block");
  		this.images = this.element.getElementsByClassName("js-cd-img");
  		this.contents = this.element.getElementsByClassName("js-cd-content");
  		this.offset = 0.8;
  		this.hideBlocks();
  	};

  	VerticalTimeline.prototype.hideBlocks = function() {
  		//hide timeline blocks which are outside the viewport
  		if ( !"classList" in document.documentElement ) {
  			return;
  		}
  		var self = this;
  		for( var i = 0; i < this.blocks.length; i++) {
  			(function(i){
  				if( self.blocks[i].getBoundingClientRect().top > window.innerHeight*self.offset ) {
  					self.images[i].classList.add("cd-is-hidden");
  					self.contents[i].classList.add("cd-is-hidden");
  				}
  			})(i);
  		}
  	};

  	VerticalTimeline.prototype.showBlocks = function() {
  		if ( ! "classList" in document.documentElement ) {
  			return;
  		}
  		var self = this;
  		for( var i = 0; i < this.blocks.length; i++) {
  			(function(i){
  				if( self.contents[i].classList.contains("cd-is-hidden") && self.blocks[i].getBoundingClientRect().top <= window.innerHeight*self.offset ) {
  					// add bounce-in animation
  					self.images[i].classList.add("cd-timeline__img--bounce-in");
  					self.contents[i].classList.add("cd-timeline__content--bounce-in");
  					self.images[i].classList.remove("cd-is-hidden");
  					self.contents[i].classList.remove("cd-is-hidden");
  				}
  			})(i);
  		}
  	};

  	var verticalTimelines = document.getElementsByClassName("js-cd-timeline"),
  		verticalTimelinesArray = [],
  		scrolling = false;
  	if( verticalTimelines.length > 0 ) {
  		for( var i = 0; i < verticalTimelines.length; i++) {
  			(function(i){
  				verticalTimelinesArray.push(new VerticalTimeline(verticalTimelines[i]));
  			})(i);
  		}

  		//show timeline blocks on scrolling
  		window.addEventListener("scroll", function(event) {
  			if( !scrolling ) {
  				scrolling = true;
  				(!window.requestAnimationFrame) ? setTimeout(checkTimelineScroll, 250) : window.requestAnimationFrame(checkTimelineScroll);
  			}
  		});
  	}

  	function checkTimelineScroll() {
  		verticalTimelinesArray.forEach(function(timeline){
  			timeline.showBlocks();
  		});
  		scrolling = false;
  	};
  })();

/**
  jQuery(function() {
  jQuery.scrollify({
		section:".page-id-193 .panel-layout > .panel-grid",
    scrollbars:false,
    before:function(i,panels) {

      var ref = panels[i].attr("id");

      jQuery(".section-pagination .active").removeClass("active");

      jQuery(".section-pagination").find("a[href=\"#" + ref + "\"]").addClass("active");
    },
    afterRender:function() {
      var pagination = "<ul class=\"section-pagination\">";
      var activeClass = "";
      jQuery(".page-id-193 .panel-layout > .panel-grid").each(function(i) {
        activeClass = "";
        if(i===0) {
          activeClass = "active";
        }
        pagination += "<li><a class=\"" + activeClass + "\" href=\"#" + jQuery(this).attr("id") + "\"></a></li>";
      });

      pagination += "</ul>";

      jQuery("#pg-193-0").append(pagination);
      /*

      Tip: The two click events below are the same:

      jQuery(".pagination a").on("click",function() {
        jQuery.scrollify.move(jQuery(this).attr("href"));
      });


      jQuery(".section-pagination a").on("click",jQuery.scrollify.move);
    }
  });
});

**/

jQuery('.register-link > a').click(function(){

jQuery('.login-wrap').fadeOut();
jQuery('.register-wrap > .has-divider > h2').css('font-size', '32px');

jQuery('.register-wrap').fadeIn();

})


jQuery('.login-link > a').click(function(){

jQuery('.register-wrap').fadeOut();
jQuery('.login-wrap > .has-divider > h2').css('font-size', '32px');

jQuery('.login-wrap').fadeIn();

});


jQuery(document).ready(function(){

  jQuery('.flex-control-nav').slick({


  slidesToShow: 4,
    slidesToScroll: 2,
    vertical: true,
    infinite: true,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        vertical: false
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]

  });


});
