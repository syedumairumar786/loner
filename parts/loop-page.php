<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<header class="article-header hide">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->

    <section class="entry-content" itemprop="articleBody">
	    <?php if ((is_page('my-account')) and (is_user_logged_in())) : ?>
<?php echo do_shortcode('[woocommerce_my_account]'); ?>
			<?php else: ?>
				<?php the_content(); ?>
			<?php endif; ?>
	</section> <!-- end article section -->

	<footer class="article-footer">
		 <?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->

	<?php comments_template(); ?>

</article> <!-- end article -->
