<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<header class="article-header hide">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->

    <section class="entry-content" itemprop="articleBody">
			<div class="page-head">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <h2><?php the_title(); ?></h2>
        </div>
    </div>
</div>
	    <?php the_content(); ?>
	</section> <!-- end article section -->
	<section class="cd-timeline js-cd-timeline">
			<div class="cd-timeline__container">
				<div class="cd-timeline__block js-cd-block">
					<div class="cd-timeline__img cd-timeline__img--picture js-cd-img">

					</div> <!-- cd-timeline__img -->

					<div class="cd-timeline__content js-cd-content">
						<h3>New QI Charger in development</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>

						<span class="cd-timeline__date">Sep 18</span>
					</div> <!-- cd-timeline__content -->
				</div> <!-- cd-timeline__block -->

				<div class="cd-timeline__block js-cd-block">
					<div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
					</div> <!-- cd-timeline__img -->

					<div class="cd-timeline__content js-cd-content">
						<h2>Title of section 2</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde?</p>

						<span class="cd-timeline__date">Jan 18</span>
					</div> <!-- cd-timeline__content -->
				</div> <!-- cd-timeline__block -->

				<div class="cd-timeline__block js-cd-block">
					<div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
					</div> <!-- cd-timeline__img -->

					<div class="cd-timeline__content js-cd-content">
						<h2>Title of section 3</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, obcaecati, quisquam id molestias eaque asperiores voluptatibus cupiditate error assumenda delectus odit similique earum voluptatem doloremque dolorem ipsam quae rerum quis. Odit, itaque, deserunt corporis vero ipsum nisi eius odio natus ullam provident pariatur temporibus quia eos repellat consequuntur perferendis enim amet quae quasi repudiandae sed quod veniam dolore possimus rem voluptatum eveniet eligendi quis fugiat aliquam sunt similique aut adipisci.</p>

						<span class="cd-timeline__date">Jan 24</span>
					</div> <!-- cd-timeline__content -->
				</div> <!-- cd-timeline__block -->

				<div class="cd-timeline__block js-cd-block">
					<div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
					</div> <!-- cd-timeline__img -->

					<div class="cd-timeline__content js-cd-content">
						<h2>Title of section 4</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>

						<span class="cd-timeline__date">Feb 14</span>
					</div> <!-- cd-timeline__content -->
				</div> <!-- cd-timeline__block -->

				<div class="cd-timeline__block js-cd-block">
					<div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
					</div> <!-- cd-timeline__img -->

					<div class="cd-timeline__content js-cd-content">
						<h2>Title of section 5</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum.</p>

						<span class="cd-timeline__date">Feb 18</span>
					</div> <!-- cd-timeline__content -->
				</div> <!-- cd-timeline__block -->

				<div class="cd-timeline__block js-cd-block">
					<div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
					</div> <!-- cd-timeline__img -->

					<div class="cd-timeline__content js-cd-content">
						<h2>Final Section</h2>
						<p>This is the content of the last section</p>
						<span class="cd-timeline__date">Feb 26</span>
					</div> <!-- cd-timeline__content -->
				</div> <!-- cd-timeline__block -->
			</div>
		</section> <!-- cd-timeline -->
	<footer class="article-footer">
		 <?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->

	<?php comments_template(); ?>

</article> <!-- end article -->
