<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

    <section class="entry-content" itemprop="articleBody">
	    <!--<?php the_content(); ?>-->

			<section class="main-section">
				<div class="grid-container">
					<div class="grid-x grid-padding-x grid-margin-x">
						<div class="large-12 cell">
							<h1>A new era of Websites</h1>
							<h3 class="tagline">We tried to change the way a Website is designed, to make it as easy as 1 2 3.</h3>
							<p>Loner will not only change the way a Website is designed, but also make sure it runs as faster.</p>
						</div>
					</div>
				</div>
			</section>
			<section class="demos">
				<div class="grid-container">
					<div class="grid-x grid-padding-x grid-margin-x">
						<div class="large-12 cell">
							<h1>Combining Design & Performance</h1>
							<h3 class="tagline">We know that performance equally matters as design in any Website.</h3>
							<p>Loner comes packed with some of the top-notch designs that are highly optimised for performance.</p>

						</div>
					</div>
				</div>
				<div class="text-center">
					<img src="<?php bloginfo('template_directory'); ?>/assets/images/demos.jpg" alt="">
				</div>
			</section>
      <section class="speed">
				<div class="grid-container">
					<div class="grid-x grid-padding-x grid-margin-x">
						<div class="large-12 cell">
							<div class="text-center">
								<h1>Built for Speed</h1>
								<h3 class="tagline">We know speed matters to you. We therefore made it faster.</h3>
								<p>Loner comes packed with some of the top-notch designs that are highly optimised for performance.</p>
							</div>

						</div>

    <div class="large-4 cell">

    <div class="section-icon">
            <svg class="icon icon-rocket"><use xlink:href="#icon-rocket"></use></svg>
        </div>
    </div><div class="large-8 cell">

    <div class="section-icon">
            <svg class="icon icon-speedometer"><use xlink:href="#icon-speedometer"></use></svg>
        </div>
    </div>

					</div>
				</div>

			</section>

	</section> <!-- end article section -->

	<footer class="article-footer">
		 <?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->

	<?php comments_template(); ?>

</article> <!-- end article -->
