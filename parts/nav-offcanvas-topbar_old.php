<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>
<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="large-12 cell">
							<div class="top-bar"   id="top-bar-menu" >
								<div class="top-bar-bg"></div>
								<div class="top-bar-left float-left">
									<ul class="menu">
										<li><a class="logo logo-image" style="background-image: url(http://syed.web/loner/wp-content/uploads/2018/09/qualis-logo.png);" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></li>
									</ul>
								</div>
								<div class="top-bar-right show-for-large">
									<?php joints_top_nav(); ?>
								</div>
								<div class="top-bar-right float-right hide-for-large">
									<ul class="menu">
									<button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
									<!-- 	<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>-->
									</ul>
								</div>
							</div>

					</div>
				</div>
			</div>
