<?php

get_header(); ?>

	<div class="content">
		<header class="article-header">
			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
		</header> <!-- end article header -->

		<div class="grid-container">
			<div class="inner-content grid-x grid-margin-x grid-padding-x">
				<div id="shop-sidebar" class="sidebar small-12 medium-3 large-3 cell" role="complementary">

					<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

						<?php dynamic_sidebar( 'sidebar1' ); ?>

					<?php else : ?>

					<!-- This content shows up if there are no widgets defined in the backend. -->

					<div class="alert help">
						<p><?php _e( 'Please activate some Widgets.', 'jointswp' );  ?></p>
					</div>

					<?php endif; ?>

				</div>
			    <main class="main small-12 large-9 medium-9 cell" role="main">

					<?php woocommerce_content(); ?>

				</main> <!-- end #main -->

			</div> <!-- end #inner-content -->
					</div>


	</div> <!-- end #content -->

<?php get_footer(); ?>
