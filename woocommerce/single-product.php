<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<div class="content">

<div class="grid-container">
  <div class="inner-content grid-x grid-margin-x grid-padding-x">

    <main class="main small-12 cell" role="main">

        <?php woocommerce_content(); ?>

    </main> <!-- end #main -->

  </div> <!-- end #inner-content -->
</div>

</div> <!-- end #content -->

<?php get_footer(); ?>
